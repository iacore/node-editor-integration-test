use std::fs::File;

use eframe::{
    egui,
    epaint::{pos2, Color32, ColorImage, ImageData, Pos2, Rect},
    glow::Buffer,
};
use serde::{Deserialize, Serialize};

fn main() -> Result<(), eframe::Error> {
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "My egui App",
        native_options,
        Box::new(|cc| Box::new(MyEguiApp::new(cc))),
    )
}

struct MyEguiApp {
    config: StubConfig,
    tex_canvas: eframe::epaint::TextureHandle,
}

const CONFIG_PATH: &str = "config.yaml";

impl MyEguiApp {
    fn new(cc: &eframe::CreationContext<'_>) -> Self {
        let config = match load_config() {
            Ok(c) => c,
            Err(e) => {
                eprintln!("Load config failed: {e}");
                Default::default()
            }
        };

        let canvas_size = [CANVAS_SIZE.x as usize, CANVAS_SIZE.y as usize];
        let tex_canvas = cc.egui_ctx.load_texture(
            "canvas",
            ColorImage::new(canvas_size, Color32::KHAKI),
            Default::default(),
        );
        Self { config, tex_canvas }
    }
    fn save_config(&self) -> anyhow::Result<()> {
        serde_yaml::to_writer(File::create(CONFIG_PATH)?, &self.config)?;
        Ok(())
    }
}

fn load_config() -> anyhow::Result<StubConfig> {
    let file = File::open(CONFIG_PATH)?;
    Ok(serde_yaml::from_reader(&file)?)
}

const CANVAS_SIZE: egui::Vec2 = egui::Vec2::splat(100.);

impl eframe::App for MyEguiApp {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            let (_, painter) = ui.allocate_painter(CANVAS_SIZE, egui::Sense::hover());
            painter.image(
                self.tex_canvas.id(),
                Rect::from_min_max(Pos2::ZERO, CANVAS_SIZE.to_pos2()),
                Rect::from_min_max(pos2(0.0, 0.0), pos2(1.0, 1.0)),
                Color32::WHITE,
            );
            ui.add(egui::Slider::new(&mut self.config.field_a, 0..=100));
            ui.text_edit_multiline(&mut self.config.field_b);
            if ui.button("save").clicked() {
                if let Err(e) = self.save_config() {
                    eprintln!("Save failed: {e}");
                }
            }
        });
    }
}

#[derive(Default, Serialize, Deserialize)]
struct StubConfig {
    field_a: i32,
    field_b: String,
}
